const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName : {
		type: String,
		required : [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	brand: {
		type: String,
		required: [true, "Brand is required"]
	},
	inventory: {
		type: Number,
		required: [true, "Inventory is required"]
	},
	category: {
		type: String,
		required: [true, "Category is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	buyer: [
	{
		userId: {
			type: String,
			required: [true, "userId is required"]
		},
		quantity: {
			type: Number,
			required: [true, "Order Quantity is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
})

module.exports = mongoose.model("Product",productSchema)