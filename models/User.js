const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required : [true, "Full Name is required"]
	},
	email: {
		type: String,
		required : [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	purchase: [{
		productId: {
			type: String,
			required: [true, "productId is required"]
		},
		status: {
			type: String,
			default : "no purchase"
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}],
})

module.exports = mongoose.model("User", userSchema)