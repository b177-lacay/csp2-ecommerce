const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth")

// Routes for register
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

    const data = {
        userId : req.params.userId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    userController.setAdmin(data).then(resultFromController => res.send(resultFromController));
});



// Orders router
// Create Order router "/users/checkout""
router.post("/checkout",auth.verify, (req,res)=>{
    if(!auth.decode(req.headers.authorization).isAdmin){
        const userData = {
            productId: req.body.productId,
            quantity: req.body.quantity, 
            userId: auth.decode(req.headers.authorization).id      
        }
userController.createOrder(userData).then(resultFromController=> res.send(resultFromController))
    } else{
        res.send('Admin is not allowed to process Order')
    }
})

// Get all order(admin only "/users/order")
router.get("/order", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    userController.getAllOrder(isAdmin).then(resultFromController => res.send(resultFromController));
})


// Get my order (non-admin "users/")
router.get("/myorders", auth.verify, (req, res) => {
    const userId = auth.decode(req.headers.authorization).id;
    userController.getuserOrder(userId).then(resultFromController => res.send(resultFromController));
})


// [STRETCH]
router.get("/",auth.verify, (req,res) => {
    if(auth.decode(req.headers.authorization).isAdmin){
        userController.getAllUser().then(resultFromController => res.send(resultFromController));
    }else res.send('Admin only')
});

router.delete("/:id" , auth.verify, (req, res) => {
    if(auth.decode(req.headers.authorization).isAdmin)
    {taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))}
else res.send(`User is not allowed to delete certain user`)
})

module.exports = router;