const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

router.post("/", auth.verify, (req,res) => { 
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.createProduct(req.body)
		.then(resultFromController => res.send(resultFromController)
	)}
		else{
			res.send(`Access denied`)}
})

/*
1. A GET request is sent to the /products endpoint.
2. API retrieves all active products and returns them in its response*/
router.get("/", (req,res) => {
	productController.getActiveProduct(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/category", (req,res) => {
	productController.getProductbyCategory(req.body).then(resultFromController => res.send(resultFromController))
})

/*
Retrieve Single Product Sample Workflow
1. A GET request is sent to the /products/:productId endpoint.
2. API retrieves product that matches productId URL parameter and returns it in its response.
*/

router.get("/:id", (req, res) =>{ 
	productController.getSingleProduct(req.params.id).then(resultFromController => res.send(resultFromController))
})
/*
Update Product Sample Workflow
1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API finds product with ID matching the productId URL parameter and overwrites its info with those from the request body.
*/
router.put("/:productId", auth.verify, (req,res) => {
	if(auth.decode(req.headers.authorization).isAdmin){productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)
		)}
		else{
			res.send(`Access denied`)
		}
	
});

/*
Archive Product Sample Workflow
1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/archive endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to false.

*/

router.put("/:productId/archive", auth.verify, (req,res) =>{
	if(auth.decode(req.headers.authorization).isAdmin){productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController)
		)} else{
		res.send(`Access denied`)
	}	
});

module.exports = router;