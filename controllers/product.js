const Product = require("../models/Product");
const auth = require("../auth");

/*
Create Product Sample Workflow
1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API creates product using the contents of the request body.
*/
module.exports.createProduct = (reqBody) =>{
	return Product.findOne({ productName: reqBody.productName}).then(result =>{
		if (result == null){
			const newProduct = new Product({
				productName : reqBody.productName,
				description : reqBody.description,
				price : reqBody.price,
				brand : reqBody.brand,
				inventory : reqBody.inventory,
				category : reqBody.category
			})
		return newProduct.save().then((product, error) => {
		if(error){
			return (`Product Creation Failed`);
		}
		else{
			return (`Product Added`);
		}
	})
}
	else{
		return(`Duplicate product`)
		}
	})
};

/*
1. A GET request is sent to the /products endpoint.
2. API retrieves all active products and returns them in its response
*/

module.exports.getActiveProduct = (reqBody) =>{
	return Product.find({isActive: true}).then(result =>{
		if(result == null){
			console.log(`No products found`)
		}
		else{
			return result;
		}
	})
};

/*
Retrieve Single Product Sample Workflow
1. A GET request is sent to the /products/:productId endpoint.
2. API retrieves product that matches productId URL parameter and returns it in its response.
*/

module.exports.getSingleProduct = (productId) =>{
	return Product.findById(productId).then((result,error) =>{
		if(error){
			console.log(error)
			return (`Invalid Id`)
		}
		else{
			return result;
		}
	})
};

/*
Update Product Sample Workflow
1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API finds product with ID matching the productId URL parameter and overwrites its info with those from the request body.
*/

module.exports.updateProduct = (reqParams, reqBody) =>{
	let updatedProduct = {
			productName : reqBody.productName,
			description : reqBody.description,
			price : reqBody.price,
			brand : reqBody.brand,
			inventory : reqBody.inventory,
			category : reqBody.category
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
				if(error){
					return (`Failed updating the product`)
				}
				else{
					return (`Succesfully updated`)
				}
	})
}	

/*

Archive Product Sample Workflow
1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/archive endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to false.*/

module.exports.archiveProduct = (reqParams) =>{
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId , updateActiveField).then((result, error) =>{
		if(error){
			return (`Error archiving the product`)
		}
		else{
			return (`Product is archived`);
		}
	})
}

// STRETCH
module.exports.getProductbyCategory = (reqBody) =>{
	return Product.find({category : reqBody.category}).then(result =>{
		if(result == null){
			console.log(`No products found`)
		}
		else{
			return result;
		}
	})
};