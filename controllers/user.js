const User = require("../models/User");
const Product = require("../models/Product");
const Orders = require("../models/Orders");
const auth = require("../auth");
const bcrypt = require("bcrypt");


//[SECTION] [CREATE] User registration
module.exports.registerUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
	const newUser = new User({
		fullName : reqBody.fullName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return (`${reqBody.fullName} ,you have registered succesfully`);
		}
	})
}
	else{
		return(`Email exists`)
		}
	})
};


//[SECTION] [RETRIEVE] User authentication (/login)

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				// Generate an access token
				return {access : auth.createAccessToken(result)}
			}
			else{
				return `Password is incorrect. Please try again`;
			}
		}
	})
};


// [SECTION] [UPDATE] Setting user as admin(Admin only)
module.exports.setAdmin = (data) => {

    if(data.isAdmin) {
        let setasAdmin = {
            isAdmin : true
        };

        return User.findByIdAndUpdate(data.userId, setasAdmin).then((user, error) => {
            if(error){
                return false;
            } 
            else{
                return (`User is now an admin`);
            }
        })
    } 
    else{
		return Promise.resolve(false);
	}

};


// Orders controller

// [SECTION] [CREATE] Create Order- Non Admin
module.exports.createOrder = async data => {
	try {
	
		let activeStatus = await Product.findById(data.productId).then(async result =>{
			if(result.isActive == false){
				return false
			}
			else{
				let userId =  data.userId
				let productId = data.productId
				let quantity =  data.quantity
				let checkInventory = await Product.findById(data.productId).then(result =>{
					if(result.inventory !== 0 && result.inventory >= data.quantity){
						result.updateOne({$set:{inventory: result.inventory - data.quantity}}).then(result =>{})
						let totalAmt = result.price * data.quantity
						let Order = new Orders({
							userId: userId,
							order:{
								productId: productId,
								quantity: quantity,
								totalAmount: totalAmt
							}
						})

						return Order.save().then((result, error)=>{
							if(error){
								return false
							}
							else{
								
								return true
							}
						})

					}
					else{
						return false
					}
				})

				let updateProduct = await Product.findById(data.productId).then(result =>{
					if(result.inventory === 0 ){
						result.updateOne({$set:{isActive: false}}).then(result => {})
					} 
					else{
						result.buyer.push({
							userId: data.userId,
							quantity: data.quantity
						})
						return result.save().then((result, error) => {
							if(error) {
								return false
							} else {
								return true
							}
						})
					}
				})

				let updateUser = await User.findById(data.userId).then(result => {
					result.purchase.push({
						productId: data.productId,
						status: "To receive"
					})
					return result.save().then((result, error) => {
						if(error) {
							return false
						} else {
							return true
						}
					})
				})

				if(checkInventory && updateProduct && updateUser){
					return true
				} else{
					return false
				}
			}
		})
		if(activeStatus){
			return (`You have succesfully added an order`)
		} else{
			return `Product is out of stock`
		}

	} catch{
		return(`Encounter an error while processing`)
	}
}

// [SECTION] [RETRIEVE] Get all order
module.exports.getAllOrder =()=>{
	return Orders.find({}).then(result => {
		return result
	})
}

// [SECTION] [RETRIEVE] Get my order
module.exports.getuserOrder = (userId) => {
    if(userId){
        return Orders.find({userId: userId}).then((orders, error) => {
            if(error){
                return false;
            }
            else{
                return orders;
            }
        }) 
    }
    else {
        return Promise.resolve(false);
    }
}

// [STRETCH]
// get all users

module.exports.getAllUser=()=>{
	return User.find({}).then(result => {
		return result;
	})
}

// delete certain user
module.exports.deleteUser = (userId) => {
	return User.findByIdAndRemove(userId).then((removedUser, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedUser;
		}
	})
}



			




